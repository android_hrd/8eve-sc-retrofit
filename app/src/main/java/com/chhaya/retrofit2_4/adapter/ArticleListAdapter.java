package com.chhaya.retrofit2_4.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chhaya.retrofit2_4.R;
import com.chhaya.retrofit2_4.api.response.AllArticlesResponse;
import com.chhaya.retrofit2_4.callback.RecyclerViewItemClickListener;


public class ArticleListAdapter
        extends RecyclerView.Adapter<ArticleListAdapter.ArticleListViewHolder> {

    private Context context;
    private AllArticlesResponse dataSet;
    private RecyclerViewItemClickListener listener;


    public ArticleListAdapter(Context context,
                              AllArticlesResponse dataSet) {
        this.context = context;
        this.dataSet = dataSet;
        try {
            this.listener = (RecyclerViewItemClickListener) context;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }

    }

    @NonNull
    @Override
    public ArticleListAdapter.ArticleListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.article_item_layout, parent, false);
        return new ArticleListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleListAdapter.ArticleListViewHolder holder, int position) {
        holder.textTitle.setText(dataSet.getArticleList().get(position).getTitle());
        holder.textDesc.setText(dataSet.getArticleList().get(position).getDescription());
        Glide.with(context).load(dataSet.getArticleList().get(position).getImage()).placeholder(R.drawable.placeholder2).into(holder.imageArticle);


    }

    @Override
    public int getItemCount() {
        return dataSet.getArticleList().size();
    }

    class ArticleListViewHolder extends RecyclerView.ViewHolder {
        TextView textTitle, textDesc, textDelete, textUpdate;
        ImageView imageArticle;
        public ArticleListViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_article_title);
            textDesc = itemView.findViewById(R.id.text_article_description);
            textDelete = itemView.findViewById(R.id.text_article_delete);
            textUpdate = itemView.findViewById(R.id.text_article_update);
            imageArticle = itemView.findViewById(R.id.image_article);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   listener.onItemClicked(getAdapterPosition());
                }
            });

        }
    }

}
