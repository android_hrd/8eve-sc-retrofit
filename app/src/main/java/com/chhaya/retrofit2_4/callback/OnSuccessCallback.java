package com.chhaya.retrofit2_4.callback;

public interface OnSuccessCallback {
    void onSuccess(String message);
}
