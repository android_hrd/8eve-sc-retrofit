package com.chhaya.retrofit2_4.callback;

public interface OnBaseCallback<T> {

    void onSuccess(T response);
    void onError(String message);

}