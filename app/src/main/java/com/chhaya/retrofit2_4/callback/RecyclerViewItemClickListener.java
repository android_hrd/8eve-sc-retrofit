package com.chhaya.retrofit2_4.callback;

public interface RecyclerViewItemClickListener {
    void onItemClicked(int position);
}
