package com.chhaya.retrofit2_4.callback;

import com.chhaya.retrofit2_4.api.response.UploadSingleImageResponse;

public interface OnImageUploadCallback {

    void onSuccess(UploadSingleImageResponse response);
    void onError(String message);

}
