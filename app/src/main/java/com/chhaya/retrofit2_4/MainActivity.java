package com.chhaya.retrofit2_4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.chhaya.retrofit2_4.api.config.ServiceGenerator;
import com.chhaya.retrofit2_4.api.response.AllArticlesResponse;
import com.chhaya.retrofit2_4.api.response.AllPostResponse;
import com.chhaya.retrofit2_4.api.service.ArticleService;
import com.chhaya.retrofit2_4.api.service.PostService;
import com.chhaya.retrofit2_4.entity.PostEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*ArticleService articleService = ServiceGenerator.createService(ArticleService.class);
        Call<AllArticlesResponse> callArticle = articleService.findAll(1, 15);

        // background:
        callArticle.enqueue(new Callback<AllArticlesResponse>() {
            @Override
            public void onResponse(Call<AllArticlesResponse> call, Response<AllArticlesResponse> response) {
                Log.d(TAG, "onResponse: " + response.body());
            }

            @Override
            public void onFailure(Call<AllArticlesResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });*/

        PostService postService = ServiceGenerator.createService(PostService.class);
        Call<List<PostEntity>> callPost = postService.findAll();

        // Working in background
        callPost.enqueue(new Callback<List<PostEntity>>() {
            @Override
            public void onResponse(Call<List<PostEntity>> call, Response<List<PostEntity>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<PostEntity> postList = response.body();
                    Log.d(TAG, "onResponse: " + postList.get(0));
                }
            }

            @Override
            public void onFailure(Call<List<PostEntity>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });


    }
}
