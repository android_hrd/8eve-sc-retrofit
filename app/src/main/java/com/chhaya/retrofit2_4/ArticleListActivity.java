package com.chhaya.retrofit2_4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.chhaya.retrofit2_4.adapter.ArticleListAdapter;
import com.chhaya.retrofit2_4.api.response.AllArticlesResponse;
import com.chhaya.retrofit2_4.api.response.UploadSingleImageResponse;
import com.chhaya.retrofit2_4.api.service.repository.ArticleRepository;
import com.chhaya.retrofit2_4.callback.OnBaseCallback;
import com.chhaya.retrofit2_4.callback.OnSuccessCallback;
import com.chhaya.retrofit2_4.callback.RecyclerViewItemClickListener;

public class ArticleListActivity extends AppCompatActivity
implements RecyclerViewItemClickListener {

    private ArticleRepository articleRepository;
    private final static String TAG = "ArticleListActivity";

    private RecyclerView rvArticleList;
    private ArticleListAdapter articleListAdapter;
    private AllArticlesResponse dataSet;
    private SwipeRefreshLayout swipeRefreshLayout;

    private boolean isView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        rvArticleList = findViewById(R.id.rv_article);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_article);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchArticles();
            }
        });

        articleRepository = new ArticleRepository();
        fetchArticles();



    }

    private void setupRecyclerView() {
        articleListAdapter = new ArticleListAdapter(this, dataSet);
        rvArticleList.setLayoutManager(new LinearLayoutManager(this));
        rvArticleList.setAdapter(articleListAdapter);
        articleListAdapter.notifyDataSetChanged();
    }

    private void fetchArticles() {
        swipeRefreshLayout.setRefreshing(true);
        articleRepository.findAllArticles(1, 15,
                new OnBaseCallback<AllArticlesResponse>() {
                    @Override
                    public void onSuccess(AllArticlesResponse response) {
                        dataSet = response;
                        setupRecyclerView();
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(String message) {
                        Toast.makeText(
                                ArticleListActivity.this,
                                message,
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    // implement option menu


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        Intent postArticleIntent = new Intent(
                ArticleListActivity.this,
                InputArticleActivity.class);

        startActivity(postArticleIntent);

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!isView) fetchArticles();
        isView = false;
    }

    @Override
    public void onItemClicked(int position) {

        // Create intent detail activity
        Intent detailIntent = new Intent(
                ArticleListActivity.this,
                ArticleDetailActivity.class
        );

        int articleId = dataSet.getArticleList().get(position)
                .getId();

        detailIntent.putExtra("articleId", articleId);

        isView = true;

        startActivity(detailIntent);

    }
}
