package com.chhaya.retrofit2_4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ContentLoadingProgressBar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chhaya.retrofit2_4.api.response.PostArticleResponse;
import com.chhaya.retrofit2_4.api.service.repository.ArticleRepository;
import com.chhaya.retrofit2_4.callback.OnBaseCallback;

public class ArticleDetailActivity extends AppCompatActivity {

    private ImageView imageArticleDetail;
    private TextView textTitleDetail;
    private TextView textDescDetail;
    private ProgressBar pbLoading;

    private int articleId;

    private ArticleRepository articleRepository;

    private void initViews() {
        imageArticleDetail = findViewById(R.id.image_article_detail);
        textTitleDetail = findViewById(R.id.text_title_detail);
        textDescDetail = findViewById(R.id.text_desc_detail);
        pbLoading = findViewById(R.id.pb_loading);
    }

    private void bindDataToViews() {
        // loading
        pbLoading.setVisibility(View.VISIBLE);
        articleRepository.findOneArticle(articleId, new OnBaseCallback<PostArticleResponse>() {
            @Override
            public void onSuccess(PostArticleResponse response) {
                if (response != null) {
                    Glide.with(ArticleDetailActivity.this)
                            .load(response.getData().getImage())
                            .placeholder(R.drawable.placeholder2)
                            .into(imageArticleDetail);
                    textTitleDetail.setText(response.getData().getTitle());
                    textDescDetail.setText(response.getData().getDescription());
                    pbLoading.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onError(String message) {
                Toast.makeText(ArticleDetailActivity.this,
                        message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        // init all views
        initViews();

        pbLoading.setVisibility(View.INVISIBLE);


        // init article repository
        articleRepository = new ArticleRepository();

        // on new intent
        onNewIntent(getIntent());

        // bind data to views
        bindDataToViews();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Bundle extras = intent.getExtras();
        if (extras != null) {
            articleId = extras.getInt("articleId");
        }

    }
}
