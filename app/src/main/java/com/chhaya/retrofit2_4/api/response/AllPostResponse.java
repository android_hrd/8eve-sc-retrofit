package com.chhaya.retrofit2_4.api.response;

import com.chhaya.retrofit2_4.entity.PostEntity;

import java.util.List;

public class AllPostResponse {
    private List<PostEntity> postList;

    public List<PostEntity> getPostList() {
        return postList;
    }

    public void setPostList(List<PostEntity> postList) {
        this.postList = postList;
    }

    @Override
    public String toString() {
        return "AllPostResponse{" +
                "postList=" + postList +
                '}';
    }
}
