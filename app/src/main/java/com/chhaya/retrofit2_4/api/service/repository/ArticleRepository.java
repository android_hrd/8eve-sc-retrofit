package com.chhaya.retrofit2_4.api.service.repository;

import android.util.Log;

import com.chhaya.retrofit2_4.api.config.ServiceGenerator;
import com.chhaya.retrofit2_4.api.response.AllArticlesResponse;
import com.chhaya.retrofit2_4.api.response.PostArticleResponse;
import com.chhaya.retrofit2_4.api.service.ArticleService;
import com.chhaya.retrofit2_4.callback.OnBaseCallback;
import com.chhaya.retrofit2_4.callback.OnSuccessCallback;
import com.chhaya.retrofit2_4.entity.ArticleEntity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private final static String TAG = "Article";
    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    public void findOneArticle(int id,
                               OnBaseCallback<PostArticleResponse> callback) {
        articleService.findOne(id).enqueue(new Callback<PostArticleResponse>() {
            @Override
            public void onResponse(Call<PostArticleResponse> call, Response<PostArticleResponse> response) {
                if (response.isSuccessful())
                    callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<PostArticleResponse> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public PostArticleResponse postArticle(ArticleEntity article, final OnSuccessCallback callback) {
        final PostArticleResponse data = new PostArticleResponse();
        articleService.postOne(article).enqueue(new Callback<PostArticleResponse>() {
            @Override
            public void onResponse(Call<PostArticleResponse> call, Response<PostArticleResponse> response) {
                if (response.isSuccessful()) {
                    data.setCode(response.body().getCode());
                    data.setMessage(response.body().getMessage());
                    data.setData(response.body().getData());
                    callback.onSuccess(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<PostArticleResponse> call, Throwable t) {
                Log.e("Article", t.getMessage());
            }
        });
        return data;
    }

    public void findAllArticles(int page, int limit, final OnBaseCallback<AllArticlesResponse> callback) {
        articleService.findAll(page, limit).enqueue(new Callback<AllArticlesResponse>() {
            @Override
            public void onResponse(Call<AllArticlesResponse> call, Response<AllArticlesResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    callback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<AllArticlesResponse> call, Throwable t) {
               callback.onError(t.getMessage());
            }
        });
    }

}
