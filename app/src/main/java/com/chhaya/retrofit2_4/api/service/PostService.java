package com.chhaya.retrofit2_4.api.service;

import com.chhaya.retrofit2_4.api.response.AllPostResponse;
import com.chhaya.retrofit2_4.entity.PostEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostService {

    @GET("posts")
    Call<List<PostEntity>> findAll();

}
