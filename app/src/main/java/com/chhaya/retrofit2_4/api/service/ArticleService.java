package com.chhaya.retrofit2_4.api.service;

import com.chhaya.retrofit2_4.api.response.AllArticlesResponse;
import com.chhaya.retrofit2_4.api.response.PostArticleResponse;
import com.chhaya.retrofit2_4.entity.ArticleEntity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<AllArticlesResponse> findAll(@Query("page") long page,
                                      @Query("limit") long limit);


    @GET("v1/api/articles/{id}")
    Call<PostArticleResponse> findOne(@Path("id") int id);


    @POST("v1/api/articles")
    Call<PostArticleResponse> postOne(@Body ArticleEntity article);

}
