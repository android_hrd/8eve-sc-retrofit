package com.chhaya.retrofit2_4.api.config;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    // Create Retrofit builder object
    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BaseUrl.BASE_URL_AMS)
            .addConverterFactory(GsonConverterFactory.create());

    // Create retrofit object via builder
    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

}
