package com.chhaya.retrofit2_4.api.response;

import com.chhaya.retrofit2_4.entity.ArticleEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllArticlesResponse {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    List<ArticleEntity> articleList;

    public AllArticlesResponse(){}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ArticleEntity> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<ArticleEntity> articleList) {
        this.articleList = articleList;
    }

    @Override
    public String toString() {
        return "AllArticlesResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", articleList=" + articleList +
                '}';
    }
}
