package com.chhaya.retrofit2_4.api.service.repository;

import android.net.Uri;
import android.os.FileUtils;
import android.util.Log;

import com.chhaya.retrofit2_4.api.config.ServiceGenerator;
import com.chhaya.retrofit2_4.api.response.UploadSingleImageResponse;
import com.chhaya.retrofit2_4.api.service.UploadImageService;
import com.chhaya.retrofit2_4.callback.OnImageUploadCallback;
import com.chhaya.retrofit2_4.callback.OnSuccessCallback;

import java.io.File;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImageRepository {

    private UploadImageService uploadImageService;

    public UploadImageRepository() {
        uploadImageService = ServiceGenerator.createService(UploadImageService.class);
    }

    public void uploadSingleImage(
            MultipartBody.Part file, final OnImageUploadCallback callback) {

        uploadImageService.uploadSingle(file).enqueue(new Callback<UploadSingleImageResponse>() {
            @Override
            public void onResponse(Call<UploadSingleImageResponse> call, Response<UploadSingleImageResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<UploadSingleImageResponse> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

}
