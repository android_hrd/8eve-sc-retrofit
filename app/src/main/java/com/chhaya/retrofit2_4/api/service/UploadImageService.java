package com.chhaya.retrofit2_4.api.service;

import com.chhaya.retrofit2_4.api.response.UploadSingleImageResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadImageService {

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<UploadSingleImageResponse> uploadSingle(@Part MultipartBody.Part file);

}
