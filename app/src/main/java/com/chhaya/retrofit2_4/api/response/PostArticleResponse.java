package com.chhaya.retrofit2_4.api.response;

import com.chhaya.retrofit2_4.entity.ArticleEntity;
import com.google.gson.annotations.SerializedName;

public class PostArticleResponse {

    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private ArticleEntity data;

    public PostArticleResponse() {}

    public PostArticleResponse(String code, String message, ArticleEntity data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArticleEntity getData() {
        return data;
    }

    public void setData(ArticleEntity data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PostArticleResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
