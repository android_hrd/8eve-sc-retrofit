package com.chhaya.retrofit2_4;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chhaya.retrofit2_4.api.response.UploadSingleImageResponse;
import com.chhaya.retrofit2_4.api.service.repository.ArticleRepository;
import com.chhaya.retrofit2_4.api.service.repository.UploadImageRepository;
import com.chhaya.retrofit2_4.callback.OnImageUploadCallback;
import com.chhaya.retrofit2_4.callback.OnSuccessCallback;
import com.chhaya.retrofit2_4.entity.ArticleEntity;
import com.kroegerama.imgpicker.BottomSheetImagePicker;
import com.kroegerama.imgpicker.ButtonType;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class InputArticleActivity extends AppCompatActivity
    implements View.OnClickListener,
        BottomSheetImagePicker.OnImagesSelectedListener {

    private final static int REQUEST_CODE = 200;

    private ImageView imageArticleInput;
    private EditText editTitleInput, editDescInput;
    private Button btnPostArticle;
    private FrameLayout imageContainer;

    private ArticleRepository articleRepository;
    private UploadImageRepository uploadImageRepository;

    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_article);

        // init view here:
        initViews();

        // register onclick with post article button and image picker:
        btnPostArticle.setOnClickListener(this);
        imageContainer.setOnClickListener(this);

        articleRepository = new ArticleRepository();
        uploadImageRepository = new UploadImageRepository();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_article_input: {
                // Upload image:
                uploadImageRepository.uploadSingleImage(
                        uploadImage(imageUri.getPath()),
                        new OnImageUploadCallback() {
                            @Override
                            public void onSuccess(UploadSingleImageResponse response) {
                                Log.e("TAG", response.toString());

                                // Post article:
                                articleRepository.postArticle(
                                        getArticleInput(response.getData()),
                                        new OnSuccessCallback() {
                                            @Override
                                            public void onSuccess(String message) {
                                                Toast.makeText(InputArticleActivity.this,
                                                        message,
                                                        Toast.LENGTH_LONG).show();
                                                finish();
                                            }
                                        }
                                );

                            }

                            @Override
                            public void onError(String message) {
                                Log.e("TAG Error", message);
                            }
                        }
                );

                break;
            }
            case R.id.image_container: {

                new BottomSheetImagePicker.Builder(getString(R.string.demo_text_title))
                        .cameraButton(ButtonType.Button)
                        .galleryButton(ButtonType.Button)
                        .singleSelectTitle(R.string.demo_text_title)
                        .peekHeight(R.dimen.row)
                        .columnSize(R.dimen.col)
                        .requestTag("single")
                        .show(getSupportFragmentManager(), null);

                break;
            }
        }
    }


    private void initViews() {
        imageArticleInput = findViewById(R.id.image_article_input);
        editTitleInput = findViewById(R.id.edit_title_input);
        editDescInput = findViewById(R.id.edit_desc_input);
        btnPostArticle = findViewById(R.id.button_article_input);
        imageContainer = findViewById(R.id.image_container);
    }

    // Get article data from input
    private ArticleEntity getArticleInput(String imagePath) {
        String title = editTitleInput.getText().toString();
        String desc = editDescInput.getText().toString();
        return new ArticleEntity(title, desc, imagePath);
    }

    private MultipartBody.Part uploadImage(String imagePath) {

        File file = new File(imagePath);
        Log.e("TAG02", imagePath);
        Log.e("TAG03", file + "");
        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        return MultipartBody.Part.createFormData("FILE", file.getName(), fileRequestBody);
    }


    @Override
    public void onImagesSelected(List<? extends Uri> list, String s) {
        imageContainer.removeAllViews();
        for (Uri uri : list) {
            ImageView iv = (ImageView) LayoutInflater.from(this).inflate(R.layout.scrollitem_image, imageContainer, false);
            imageContainer.addView(iv);
            Glide.with(this).load(uri).into(iv);
            imageUri = uri;
            Log.e("TAG LOG", imageUri.toString());
        }
    }
}
